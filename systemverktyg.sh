#!/bin/bash

#Color variables for altering text outputs
RED='\033[0;91m' #Used for usable commands
GREEN='\033[0;92m' #Used for non-changeable information fields
BLUE='\033[0;34m' #Used for folders
NC='\033[0m' #No color / reset

#Globally used variabels
CASE_CHOICE="" #Used for various case-statement choices

#Geometric variables for structre of panels etc
WIDTH=62 #Determine width of panel lines

#Prints lines that makes up the panels
printLine(){
    LINE="-" #This/these symbols will make up the lines
    #LINE="❄"
    #LINE="(͡° ͜ʖ ͡°)"
    
    #Writes LINE an amount of times equal to WIDTH / number of symbols in LINE
    for (( i=0; i<$WIDTH/${#LINE}; i++ ))
    do
	echo -n "$LINE"
    done
    echo ""
}

#Places titles in the center of the panel width
centerAlignTitle(){
    TITLE=$1 #TITLE holds the string argument passed to the function
    TITLELEN=${#TITLE} #TITLELEN holds the amount of symbols in TITLE
    
    CENTER=$(( WIDTH/2 - TITLELEN/2 ))

    #If a string uses escape characters; increase CENTER by some amount
    if [[ $TITLE == *"\033"* ]]
    then
	CENTER=$(( CENTER+7 ))
    fi

    #Prints CENTER amount of spaces
    for (( i=0; i<$CENTER; i++ ))
    do
	echo -n " "
    done
    echo -e "$TITLE"
}

#Waits for user to press enter before continuing
waitForEnter(){
    SCRAP=""
    echo "Press enter to continue..."
    read SCRAP
}

#Just writes the new line on which user input is typed
commandLine(){
    echo -n "> "
}

groupAdd(){
    NEWGROUP=""
    
    clear
    echo "Enter name of new group: "
    commandLine
    read NEWGROUP
    sudo groupadd $NEWGROUP && echo "New group $NEWGROUP added"
    
    waitForEnter
}

groupList(){
    clear
    echo -e "${GREEN}$(getent group | cut -d: -f1)${NC}"
    
    waitForEnter
}

#Prints the info for groupView and groupModify
groupViewExist(){
    printLine
    centerAlignTitle "SYSTEM MODIFIER - group:view ($GROUPNAME)"
    printLine
    echo -e "${GREEN}Group name${NC}		$(grep $GROUPNAME /etc/group | cut -d: -f1)"
    echo -e "${GREEN}Password${NC}		$(grep $GROUPNAME /etc/group | cut -d: -f2)"
    echo -e "${GREEN}GID${NC}			$(grep $GROUPNAME /etc/group | cut -d: -f3)"
    echo -e "${GREEN}Users${NC}			$(grep $GROUPNAME /etc/group | cut -d: -f4)"
    printLine
}

groupView(){
    GROUPNAME=""
    
    clear
    echo -e "${GREEN}$(getent group | cut -d: -f1)${NC}"
    echo "Which group would you like to view?"
    commandLine
    read GROUPNAME
    
    case $(grep -c $GROUPNAME /etc/group) in
	"0") echo "Group $GROUPNAME does not exist";;
	"1") clear && groupViewExist;;
    esac
    
    waitForEnter
}

groupModify(){
    GROUPNAME=""
    GROUPMODRUNNING=0 #Controls the while-loop in the function
    
    clear
    echo -e "${GREEN}$(getent group | cut -d: -f1)${NC}"
    echo "Which group would you like to modify?"
    commandLine
    read GROUPNAME

    case $(grep -c $GROUPNAME /etc/group) in
	"0") echo "Group $GROUPNAME does not exist"
	     waitForEnter
	     return;;
    esac

    while [ $GROUPMODRUNNING == 0 ]
    do
	clear
	groupViewExist
	centerAlignTitle "SYSTEM MODIFIER - group:modify ($GROUPNAME)"
	printLine
	echo -e "${RED}group:add${NC}		Add user to group"
	echo -e "${RED}group:rm${NC}		Remove user from group"
	echo ""
	echo -e "${RED}exit${NC}			Exit menu"
	printLine

	USERNAME=""

	commandLine
	read CASE_CHOICE
	case $CASE_CHOICE in
	    "group:add")echo "Which user would you like to add?"
			commandLine
			read USERNAME
			sudo usermod -a -G $GROUPNAME $USERNAME && echo "$USERNAME added to $GROUPNAME"
			waitForEnter;;

	    "group:rm")echo "Which user would you like to remove?"
		       commandLine
		       read USERNAME
		       sudo gpasswd -d $USERNAME $GROUPNAME
		       waitForEnter;;

	    "exit") ((GROUPMODRUNNING++));;
	    *) echo "Sorry, I didn't understand that" && waitForEnter
	esac
    done
}

userAdd(){
    NEWUSER=""
    
    clear
    echo "Enter name of new user"
    commandLine
    read NEWUSER
    sudo useradd $NEWUSER && echo "New user $NEWUSER added"
    
    waitForEnter
}

userList(){
    clear
    echo -e "${GREEN}$(getent passwd | cut -d: -f1)${NC}"
    
    waitForEnter
}

#Prints the info for userView and userModify
userViewExist(){
    printLine
    centerAlignTitle "SYSTEM MODIFIER - user:view ($USERNAME)"
    printLine
    echo -e "${GREEN}Username${NC}		$(getent passwd $USERNAME | cut -d: -f1)"
    echo -e "${GREEN}Password${NC}		$(getent passwd $USERNAME | cut -d: -f2)"
    echo -e "${GREEN}User ID${NC} 		$(getent passwd $USERNAME | cut -d: -f3)"
    echo -e "${GREEN}Group ID${NC}		$(getent passwd $USERNAME | cut -d: -f4)"
    echo -e "${GREEN}Comment${NC} 		$(getent passwd $USERNAME | cut -d: -f5)"
    echo -e "${GREEN}Directory${NC}		$(getent passwd $USERNAME | cut -d: -f6)"
    echo -e "${GREEN}Shell${NC}   		$(getent passwd $USERNAME | cut -d: -f7)"
    printLine
}

userView(){
    USERNAME=""
    
    clear
    echo -e "${GREEN}$(getent passwd | cut -d: -f1)${NC}"
    echo  "Which user would you like to view?"
    commandLine
    read USERNAME
    
    case $(grep -c $USERNAME /etc/passwd) in
	"0") echo "User $USERNAME does not exist"
	     waitForEnter
	     return;;
	"1") clear && userViewExist;;
    esac
    
    waitForEnter
}

userModify(){
    USERNAME=""
    USERMODRUNNING=0 #Used to control the while-loop
    
    clear
    echo -e "${GREEN}$(cat /etc/passwd | cut -d: -f1)${NC}"
    echo "Which user would you like to modify?"
    commandLine
    read USERNAME
    
    case $(grep -c $USERNAME /etc/passwd) in
	"0") echo "user $USERNAME does not exist"
	     waitForEnter
	     return ;;
    esac

    while [ $USERMODRUNNING == 0 ]
    do
	clear
	userViewExist
	centerAlignTitle "SYSTEM MODIFIER - user:modify ($USERNAME)"
	printLine
	echo -e "${RED}mod:username${NC}		Change username"
	echo -e "${RED}mod:password${NC}		Change password"
	echo -e "${RED}mod:uid${NC}			Change user ID"
	echo -e "${RED}mod:gid${NC}			Change group ID"
	echo -e "${RED}mod:comment${NC}		Change user comment"
	echo -e "${RED}mod:dir${NC}			Change default directory"
	echo -e "${RED}mod:shell${NC}		Change default shell"
	echo ""
	echo -e "${RED}exit${NC}                    Exit menu"
	printLine

	CASE_CHOICE=""

	commandLine
	read CASE_CHOICE
	case $CASE_CHOICE in
	    "mod:username") echo "Enter new username"
			    commandLine
			    read CASE_CHOICE 
			    sudo usermod -l $CASE_CHOICE $USERNAME && echo "Username $USERNAME changed to $CASE_CHOICE"
			    USERNAME=$CASE_CHOICE
			    waitForEnter;;

	    "mod:password") echo "Enter new password for $USERNAME"
			    commandLine
			    sudo passwd $OLDCHOICE
			    waitForEnter;;

	    "mod:uid") echo "Enter new user ID for $USERNAME"
		       commandLine
		       read CASE_CHOICE
		       sudo usermod -u $CASE_CHOICE $USERNAME && echo "User ID succesufully changed"
		       waitForEnter;;

	    "mod:gid") echo "Enter new group ID for $USERNAME"
		       commandLine
		       read CASE_CHOICE
		       sudo usermod -g $CASE_CHOICE $USERNAME && echo "Group ID succesfully changed"
		       waitForEnter;;

	    "mod:comment") echo "Enter new comment for $USERNAME"
			   commandLine
			   read -r CASE_CHOICE
			   sudo usermod -c "$CASE_CHOICE" $USERNAME && echo "Comment succesfully changed"
			   waitForEnter;;

	    "mod:dir") echo "Enter path to new default directory for $USERNAME"
		       commandLine
		       read CASE_CHOICE
		       sudo usermod -m -d $CASE_CHOICE $USERNAME && echo "Default directory changed to $CASE_CHOICE"
		       waitForEnter;;

	    "mod:shell") echo "Enter new default shell for $USERNAME"
			 commandLine
			 read CASE_CHOICE
			 sudo usermod -s $CASE_CHOICE $USERNAME && echo "Default shell changed to $CASE_CHOICE"
			 waitForEnter;;

	    "exit") ((USERMODRUNNING++));;

	    *) echo "Sorry, I didn't understand that"
	       waitForEnter
	esac
    done
}

folderAdd(){
    NEWFOLDER=""
    
    clear
    echo "Enter name for new folder"
    commandLine
    read NEWFOLDER
    mkdir $NEWFOLDER && echo -e "Created folder ${BLUE}$NEWFOLDER${NC}"
    
    waitForEnter
}

folderList(){
    FOLDERPATH=""
    
    clear
    printLine
    echo -e "${GREEN}Current directory${NC} $(pwd)"
    printLine
    echo -e "${BLUE}$(ls -d */)${NC}"
    printLine
    echo "Select folder"
    commandLine
    read FOLDERPATH

    clear
    printLine
    centerAlignTitle "Content of ${BLUE}$FOLDERPATH${NC}/"
    printLine
    echo -e "$(ls --color -F $FOLDERPATH)"
    printLine
    
    waitForEnter
}

#Prints the info for folderView and folderModify
folderViewExist(){
    printLine
    centerAlignTitle "Attriubtes for ${BLUE}$FOLDERPATH/${NC}"
    printLine
    ls --color -al $FOLDERPATH
    printLine
}

folderView(){
    FOLDERPATH=""
    
    clear
    printLine
    echo -e "${GREEN}Current directory${NC} $(pwd)"
    printLine
    echo -e "${BLUE}$(ls -d */)${NC}"
    printLine
    echo "Select folder"
    commandLine
    read FOLDERPATH
    
    clear
    folderViewExist
    
    waitForEnter
}

folderModifyOwner(){
    echo -e "Enter new owner of ${BLUE}$FOLDERPATH${NC}"
    commandLine
    read CASE_CHOICE
    sudo chown -R $CASE_CHOICE $FOLDERPATH && echo "Owner changed to $CASE_CHOICE"
    
    waitForEnter
}

privilegeChoice(){
    FOLDERRUNNING=0

    case $CASE_CHOICE in
	"priv:owner") TEMP=u ;;
	"priv:group") TEMP=g ;;
	"priv:other") TEMP=o ;;
    esac

    while [ $FOLDERRUNNING == 0 ]
    do
        clear
	folderViewExist
	centerAlignTitle "SYSTEM MODIFIER - folder modify ($TEMP) ($FOLDERPATH)"
	printLine
	echo -e "${RED}enable:read${NC}		Enable read privileges"
	echo -e "${RED}enable:write${NC}		Enable write privileges"
	echo -e "${RED}enable:execute${NC}		Enable execute privileges"
	echo ""
	echo -e "${RED}disable:read${NC}		Disable read privileges"
	echo -e "${RED}disable:write${NC}		Disable write privileges"
	echo -e "${RED}disable:execute${NC}		Disable execute privileges"
	echo ""
	echo -e "${RED}exit${NC}			Exit menu"
	printLine

	PRIVCHANGEPROMPT="Priviliges has been changed for $FOLDERPATH"

	commandLine
	read CASE_CHOICE
	case $CASE_CHOICE in
	    "enable:read") sudo chmod -R $TEMP+r $FOLDERPATH && echo -e PRIVCHANGEPROMPT;;
	    "enable:write") sudo chmod -R $TEMP+w $FOLDERPATH && echo -e PRIVCHANGEPROMPT;;
	    "enable:execute") sudo chmod -R $TEMP+x $FOLDERPATH && echo -e PRIVCHANGEPROMPT;;
	    "disable:read") sudo chmod -R $TEMP-r $FOLDERPATH && echo -e PRIVCHANGEPROMPT;;
	    "disable:write") sudo chmod -R $TEMP-w $FOLDERPATH && echo -e PRIVCHANGEPROMPT;;
	    "disable:execute") sudo chmod -R $TEMP-x $FOLDERPATH && echo -e PRIVCHANGEPROMPT;;
	    "exit")((FOLDERRUNNING++));;
	    *) echo "Sorry I didn't understand that."
	       waitForEnter;;
	esac
    done
}

folderModifyPrivilege(){
    clear
    printLine
    centerAlignTitle "SYSTEM MODIFIER - folder modify ($FOLDERPATH)"
    printLine
    echo -e "${RED}priv:owner${NC}		Change privilege for owner"
    echo -e "${RED}priv:group${NC}		Change privilege for group"
    echo -e "${RED}priv:other${NC}		change privilege for other"
    echo ""
    echo -e "${RED}exit${NC}			Exit menu"
    printLine

    commandLine
    read CASE_CHOICE
    case $CASE_CHOICE in
	"priv:owner") privilegeChoice;;
	"priv:group") privilegeChoice;;
	"priv:other") privilegeChoice;;
	"exit") return;;
	*) echo "Sorry, I didn't unserstand that"
	   waitForEnter;;
    esac
}

folderModify(){
    FOLDER_MODIFY_RUNNING=0
    FOLDERPATH=""
    
    clear
    printLine
    echo -e "${GREEN}Current directory${NC} $(pwd)"
    printLine
    echo -e "${BLUE}$(ls -d */)${NC}"
    printLine
    echo "Select folder"
    commandLine
    read FOLDERPATH

    while [ $FOLDER_MODIFY_RUNNING == 0 ]
    do
	
	clear
	folderViewExist
	centerAlignTitle "SYSTEM MODIFIER - folder modify ($FOLDERPATH)/"
	printLine
	echo -e "${RED}mod:owner${NC}		Change owner"
	echo -e "${RED}mod:priv${NC}		Change folder privilages"
	echo ""
	echo -e "${RED}mod:sticky${NC}		Set folder sticky bit"
	echo -e "${RED}mod:rm:sticky${NC}		Remove folder sticky bit"
	echo ""
	echo -e "${RED}mod:sgid${NC}		Set GID for folder"
	echo -e "${RED}mod:rm:sgid${NC}		Remove GID for folder"
	echo ""
	echo -e "${RED}mod:date${NC}		Change last modified time stamp"
	echo ""
	echo -e "${RED}exit${NC}			Exit menu"
	printLine

	DATE=""
	
	commandLine
	read CASE_CHOICE
	case $CASE_CHOICE in
	    "mod:owner") folderModifyOwner;;
	    "mod:priv") folderModifyPrivilege;;

	    "mod:sticky") sudo chmod o+t $FOLDERPATH && echo "Sticky bit set for $FOLDERPATH"
			  waitForEnter;;
	    
            "mod:rm:sticky") sudo chmod o-t $FOLDERPATH && echo "Sticky removed for $FOLDERPATH"
			     waitForEnter;;

	    "mod:sgid") sudo chmod g+s $FOLDERPATH && echo "Set GID enabled for $FOLDERPATH"
			waitForEnter;;

	    "mod:rm:sgid") sudo chmod g-s $FOLDERPATH && echo "Removed GID for $FOLDERPATH"
			   waitForEnter;;

	    "mod:date") echo "Enter new date (YYYYMMDDHHmm) for $FOLDERPATH"
			commandLine
			read DATE
			touch -t $DATE $FOLDERPATH && echo "Timestamp for $FOLDERPATH changed to $DATE"
			stat $FOLDERPATH | tail -3
			waitForEnter;;

	    "exit") ((FOLDER_MODIFY_RUNNING++));;
	    *) echo "Sorry, I didn't understand that"
	       waitForEnter;;
	esac
    done
}

MAIN_RUNNING=0 #Used to control the main program while-loop

while [ $MAIN_RUNNING == 0 ]
do
    clear
    printLine
    centerAlignTitle "SYSTEM MODIFIER"
    printLine
    echo -e "${RED}group:add${NC} 		Create a new group"
    echo -e "${RED}group:list${NC}		List system groups"
    echo -e "${RED}group:view${NC}		List user associations for group"
    echo -e "${RED}group:modify${NC}		Modify user associations for group"
    echo ""
    echo -e "${RED}user:add${NC}  		Create new user"
    echo -e "${RED}user:list${NC} 		List system users"
    echo -e "${RED}user:view${NC} 		Vire user properties"
    echo -e "${RED}user:modify${NC}		Modify user properties"
    echo ""
    echo -e "${RED}folder:add${NC}		Create a new folder"
    echo -e "${RED}folder:list${NC}		List folder contents"
    echo -e "${RED}folder:view${NC}		View folder properties"
    echo -e "${RED}folder:modify${NC}		Modify folder properties"
    echo ""
    echo -e "${RED}ssh:install${NC}		Install SSH server"
    echo -e "${RED}ssh:uninstall${NC}	        Uninstall SSH server"
    echo -e "${RED}ssh:start${NC}	        Start the SSH server"
    echo -e "${RED}ssh:stop${NC}	        Stop the SSH server"
    echo -e "${RED}ssh:status${NC}		Status for the SSH server"
    echo ""
    echo -e "${RED}exit${NC}			Exit program"
    printLine

    commandLine
    read CASE_CHOICE
    case $CASE_CHOICE in
	"group:add") groupAdd;;
	"group:list") groupList;;
	"group:view") groupView;;
	"group:modify") groupModify;;
	"user:add") userAdd;;
	"user:list") userList;;
	"user:view") userView;;
	"user:modify") userModify;;
	"folder:add") folderAdd;;
	"folder:list") folderList;;
	"folder:view") folderView;;
	"folder:modify") folderModify;;
	"ssh:install") sudo apt install openssh-server
		       waitForEnter;;
	"ssh:uninstall") sudo apt remove openssh-server
			 waitForEnter;;

	"ssh:start") sudo systemctl start sshd.service
		     systemctl --no-ask-password status sshd.service | head -3 | tail -1
		     waitForEnter;;

	"ssh:stop") sudo systemctl stop sshd.service
		    systemctl --no-ask-password status sshd.service | head -3 | tail -1
		    waitForEnter;;

	"ssh:status") systemctl --no-ask-password status sshd.service
		      waitForEnter;;

	"exit") ((MAIN_RUNNING++));;
	*) echo "Sorry, I didn't understand that"
	   waitForEnter;;
    esac
done
